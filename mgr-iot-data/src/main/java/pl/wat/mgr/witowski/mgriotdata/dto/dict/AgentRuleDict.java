package pl.wat.mgr.witowski.mgriotdata.dto.dict;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentRuleDict {
    private String typeCode;
    private String typeConfig;
}
