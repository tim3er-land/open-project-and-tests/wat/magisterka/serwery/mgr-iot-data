package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.CollectionUtils;
import pl.wat.mgr.witowski.mgriotdata.dao.DataDao;
import pl.wat.mgr.witowski.mgriotdata.dto.WhereDto;
import pl.wat.mgr.witowski.mgriotdata.enums.ConditionSigh;
import pl.wat.mgr.witowski.mgriotdata.event.ProcessEvent;
import pl.wat.mgr.witowski.mgriotdata.listener.interfaces.BasicDataListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ProcessedListener<T> extends BasicDataListener<T> {
    public ProcessedListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher) {
        super(objectMapper, applicationEventPublisher);
    }

    protected HashMap<String, Object> getSelect(String rulSetting, ProcessEvent data) throws JsonProcessingException {
        String select = extrackSelect(rulSetting);
        HashMap<String, Object> hashMap = objectMapper.readValue(data.getSaveData(), HashMap.class);
        if ("*".equalsIgnoreCase(select.trim())) {
            return hashMap;
        }
        hashMap = (HashMap<String, Object>) hashMap.entrySet().stream().filter(map -> select.contains(map.getKey())).collect(Collectors.toMap(
                map -> map.getKey(), map -> map.getValue()
        ));
        return hashMap;
    }

    protected List<WhereDto> getWhere(String rulSetting) throws JsonProcessingException {
        String where = extrackWhere(rulSetting);
        List<WhereDto> whereDtos = new ArrayList<>();
        if (rulSetting.contains("AND")) {
            whereDtos.addAll(prepareWhereCondition(Arrays.asList(where.split("AND")), ConditionSigh.AND));
        }
        if (rulSetting.contains("OR")) {
            whereDtos.addAll(prepareWhereCondition(Arrays.asList(where.split("OR")), ConditionSigh.OR));
        }
        if (CollectionUtils.isEmpty(whereDtos)) {
            whereDtos.addAll(prepareWhereCondition(Arrays.asList(where), ConditionSigh.AND));
        }
        return whereDtos;
    }

    private List<WhereDto> prepareWhereCondition(List<String> conditionList, ConditionSigh conditionSigh) {
        return conditionList.stream().map(s -> convertCondition(s, conditionSigh)).collect(Collectors.toList());
    }

    private WhereDto convertCondition(String where, ConditionSigh conditionSigh) {
        if (where.contains(">")) {
            return prepateWhereDto(where, ">", conditionSigh);
        }
        if (where.contains(">=")) {
            return prepateWhereDto(where, ">=", conditionSigh);
        }
        if (where.contains("<")) {
            return prepateWhereDto(where, "<", conditionSigh);
        }
        if (where.contains("<=")) {
            return prepateWhereDto(where, "<=", conditionSigh);
        }
        if (where.contains("=")) {
            return prepateWhereDto(where, "=", conditionSigh);
        }
        if (where.contains("<>")) {
            return prepateWhereDto(where, "<>", conditionSigh);
        }
        return null;
    }

    private WhereDto prepateWhereDto(String where, String sighCondition, ConditionSigh conditionList) {
        WhereDto.WhereDtoBuilder builder = WhereDto.builder();
        builder.condition(sighCondition);
        String[] whereSplit = where.split(sighCondition);
        builder.value(whereSplit[1].trim());
        builder.name(whereSplit[0].trim());
        builder.conditionSigh(conditionList);
        return builder.build();

    }

    protected boolean checkTopicValidation(String topicData, String topic) {
        String topicExp = topic.trim()
                .replaceAll("\\$", "\\\\\\$")
                .replaceAll("\\+", "[^/]+")
                .replaceAll("/\\#$", "(\\$|/.+)");

        Pattern pattern = Pattern.compile(topicExp);
        boolean match = pattern.matcher(topicData).matches();
        return match;
    }

    protected String extrackSelect(String rulSetting) {
        return StringUtils.substringBetween(rulSetting, "SELECT", "FROM");
    }

    protected String extrackTopic(String rulSetting) {
        if (rulSetting.contains("WHERE"))
            return StringUtils.substringBetween(rulSetting, "FROM", "WHERE");
        else
            return StringUtils.substringAfter(rulSetting, "FROM");
    }

    protected String extrackWhere(String rulSetting) {
        if (rulSetting.contains("INTO"))
            return StringUtils.substringBetween(rulSetting, "WHERE", "INTO");
        else
            return StringUtils.substringAfter(rulSetting, "WHERE");
    }

    protected boolean checkCondition(HashMap<String, Object> select, List<WhereDto> conditionList) {
        boolean condition = true;
        for (WhereDto whereDto : conditionList) {
            if (whereDto != null)
                if (select.containsKey(whereDto.getName())) {
                    Object o = select.get(whereDto.getName());
                    if (whereDto.getConditionSigh().equals(ConditionSigh.AND)) {
                        condition = condition && getConditiionValue(whereDto, o);
                    } else {
                        condition = condition || getConditiionValue(whereDto, o);
                    }
                }
        }
        return condition;
    }

    private boolean getConditiionValue(WhereDto whereDto, Object o) {
        if (whereDto.getCondition().equalsIgnoreCase(">")) {
            Double value = Double.valueOf(whereDto.getValue());
            return (double) o > value;
        }
        if (whereDto.getCondition().equalsIgnoreCase(">=")) {
            Double value = Double.valueOf(whereDto.getValue());
            return (double) o >= value;
        }
        if (whereDto.getCondition().equalsIgnoreCase("<")) {
            Double value = Double.valueOf(whereDto.getValue());
            return (double) o < value;
        }
        if (whereDto.getCondition().equalsIgnoreCase("<=")) {
            Double value = Double.valueOf(whereDto.getValue());
            return (double) o <= value;
        }
        if (whereDto.getCondition().equalsIgnoreCase("=")) {
            Double value = Double.valueOf(whereDto.getValue());
            return (double) o == value;
        }
        if (whereDto.getCondition().equalsIgnoreCase("<>")) {
            Double value = Double.valueOf(whereDto.getValue());
            return (double) o != value;
        }
        return false;
    }

    protected DataDao processed(String rulSetting, ProcessEvent data) throws Exception {
        DataDao dataDao = new DataDao();
        HashMap<String, Object> select = getSelect(rulSetting, data);
        List<WhereDto> conditionList = getWhere(rulSetting);
        if (!checkCondition(select, conditionList)) {
            return null;
        }
        dataDao.setPayload(objectMapper.writeValueAsString(select));
        String extrackTopicInto = extrackTopicInto(rulSetting);
        dataDao.setTopic(StringUtils.trim(extrackTopicInto));
        return dataDao;
    }

    private String extrackTopicInto(String rulSetting) {
        if (rulSetting.contains("INTO"))
            return StringUtils.substringAfter(rulSetting, "INTO");
        else
            return null;
    }

    protected String prepareRule(String rulSetting) {
        rulSetting = rulSetting.replace("select", "SELECT");
        rulSetting = rulSetting.replace("from", "FROM");
        rulSetting = rulSetting.replace("where", "WHERE");
        rulSetting = rulSetting.replace("into", "INTO");
        return rulSetting;
    }
}
