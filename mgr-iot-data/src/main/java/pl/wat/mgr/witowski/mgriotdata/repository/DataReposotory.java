package pl.wat.mgr.witowski.mgriotdata.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.mgr.witowski.mgriotdata.dao.DataDao;

import java.time.LocalDateTime;
import java.util.List;

public interface DataReposotory extends MongoRepository<DataDao, String> {

    @Query("{ 'agentUid' : ?0 }")
    List<DataDao> getAgentData(@Param("agentUid") String agentUid, Pageable pageable);

    @Query("{ 'agentUid' : {$in: ?0} }")
    List<DataDao> getAgentData(@Param("agentUid") List<String> agentUid);

    @Query("{ 'agentUid' : {$in: ?0} , 'creationDate' :{ $gte : ?1} }")
    List<DataDao> getAgentData(@Param("agentUid") List<String> agentUid, @Param("date") LocalDateTime date);

    @Query("{ 'agentUid' :?0, 'topic' :?1 }")
    List<DataDao> getAgentDataByTopic(@Param("agentUid") String agentUid,
                                      @Param("topicUid") String topicUid, Pageable pageable);
}
