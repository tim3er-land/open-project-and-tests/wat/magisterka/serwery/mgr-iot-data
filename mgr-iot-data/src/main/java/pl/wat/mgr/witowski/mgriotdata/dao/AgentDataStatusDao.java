package pl.wat.mgr.witowski.mgriotdata.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.wat.mgr.witowski.mgriotdata.commons.exceptions.dto.ErrorDto;

import javax.persistence.Id;
import java.util.List;

@Data
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class AgentDataStatusDao {
    @Id
    private String messageStatusUid;
    private String messageStatusType;
    private String messageUid;
    private String agentUid;
    private List<ErrorDto> errors;
    private String code;
    private String subCode;
    private String dataInsert;
}
