package pl.wat.mgr.witowski.mgriotdata.security;

import com.google.common.base.Splitter;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public class SecurityUtils {

    public static final int BLOCK_SIZE = 225;
    public static final String SEPERATOR = ",";

    public static String encrypt(String data, byte[] key) throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        org.bouncycastle.crypto.params.AsymmetricKeyParameter publicKey = PublicKeyFactory.createKey(key);
        AsymmetricBlockCipher e = new RSAEngine();
        e = new org.bouncycastle.crypto.encodings.PKCS1Encoding(e);
        e.init(true, publicKey);
        StringBuilder encrypedMessage = new StringBuilder();
        Iterable<String> strings = Splitter.fixedLength(BLOCK_SIZE).split(data);
        List<String> encodes = new ArrayList<>();
        for (String d : strings) {
            byte[] messageBytes = d.getBytes();
            byte[] hexEncodedCipher = e.processBlock(messageBytes, 0, messageBytes.length);
            encodes.add(new String(Base64.getEncoder().encodeToString(hexEncodedCipher)));
        }
        return encodes.stream().collect(Collectors.joining(SEPERATOR));
    }

    public static String decrypt(String data, byte[] privateKey) throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

        org.bouncycastle.crypto.params.AsymmetricKeyParameter privKey = PrivateKeyFactory.createKey(privateKey);
        AsymmetricBlockCipher d = new RSAEngine();
        d = new org.bouncycastle.crypto.encodings.PKCS1Encoding(d);
        d.init(false, privKey);
        StringBuilder encrypedMessage = new StringBuilder();
        String[] split = data.split(SEPERATOR);
        for (String s : split) {
            byte[] messageBytes = Base64.getDecoder().decode(s.getBytes());
            byte[] bytes = d.processBlock(messageBytes, 0, messageBytes.length);
            encrypedMessage.append(new String(bytes));
        }
        return encrypedMessage.toString();
    }

    public static PublicKey getPublicKey(String base64PublicKey) {
        PublicKey publicKey = null;
        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
            return publicKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public static String sign(String plainText, PrivateKey privateKey) throws Exception {
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(privateKey);
        privateSignature.update(plainText.getBytes(UTF_8));

        byte[] signature = privateSignature.sign();

        return Base64.getEncoder().encodeToString(signature);
    }

    public static PrivateKey getPrivateKey(String base64PrivateKey) {
        PrivateKey privateKey = null;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    public static String encodeStringToBase64(String s) {
        return Base64.getEncoder().encodeToString(s.getBytes(UTF_8));
    }


    public static boolean verify(String publicKey, String body, String sign) throws Exception {
        Signature publicSignature = Signature.getInstance("SHA256withRSA");
        publicSignature.initVerify(getPublicKey(publicKey));
        publicSignature.update(body.getBytes(UTF_8));
        byte[] signatureBytes = Base64.getDecoder().decode(sign.getBytes(UTF_8));
        return publicSignature.verify(signatureBytes);
    }
}
