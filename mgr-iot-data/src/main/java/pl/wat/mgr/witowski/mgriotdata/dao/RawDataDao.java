package pl.wat.mgr.witowski.mgriotdata.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@Document
@NoArgsConstructor
@AllArgsConstructor
public class RawDataDao {
    @Id
    private String dataUid;
    private String agentUid;
    private String payload;
    private String topic;
    private LocalDateTime creationDate;
}
