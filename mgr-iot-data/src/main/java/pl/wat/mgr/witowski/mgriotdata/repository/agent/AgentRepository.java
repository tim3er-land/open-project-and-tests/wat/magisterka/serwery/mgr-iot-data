package pl.wat.mgr.witowski.mgriotdata.repository.agent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentEntity;

import java.util.List;

public interface AgentRepository extends JpaRepository<IotAgentEntity, Long> {
    IotAgentEntity findByIotAgtUid(String agentUid);

    @Query("select a from IotAgentEntity a join a.iotAgentUsersByIotAgtId au where a.iotAgtUid = :agentUid")
    IotAgentEntity findByIotAgtUidJoinUser(@Param("agentUid") String agentUid);

    @Query("select ae from IotAgentEntity ae join ae.iotAgentUsersByIotAgtId aue where ae.iotAgtAct = 1 and aue.iotUsrUid like :uuid ")
    List<IotAgentEntity> getUserIotAgents(@Param("uuid") String uuid);
}
