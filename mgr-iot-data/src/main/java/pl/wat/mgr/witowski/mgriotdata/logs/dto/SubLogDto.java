package pl.wat.mgr.witowski.mgriotdata.logs.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

/**
 * @author: Piotr Witowski
 * @date: 04.02.2022
 * @project tim3erland-iot
 * Day of week: piątek
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubLogDto {
    private Long dur;
    private String deviceUid;
    private HashMap<String, Object> payload;
    private String deviceType;
    private String topic;
    private String op;
    private String logStatus;
    private List<String> eventList;
    private List<String> sendAgentUid;
    private String dataType;
    private String exceptionMessage;
}
