package pl.wat.mgr.witowski.mgriotdata.config;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;
import pl.wat.mgr.witowski.mgriotdata.socket.WebSocketReceiver;

/**
 * @author: Piotr Witowski
 * @date: 25.12.2021
 * @project mgr-iot-data
 * Day of week: sobota
 */
@Configuration
@EnableWebSocket
public class WebSocketConfiguration implements WebSocketConfigurer {

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new WebSocketReceiver(getApplicationEventPublisher(), DataType.DATA), "/data");
        registry.addHandler(new WebSocketReceiver(getApplicationEventPublisher(), DataType.CONFIG), "/config");
    }

    private ApplicationEventPublisher getApplicationEventPublisher() {
        return ApplicationContextProvider.getContext();
    }
}
