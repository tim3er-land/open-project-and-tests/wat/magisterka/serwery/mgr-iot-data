package pl.wat.mgr.witowski.mgriotdata.enums;

public enum DataType {

    DATA("Data"),CONFIG("Config");

    private String type;

    DataType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
