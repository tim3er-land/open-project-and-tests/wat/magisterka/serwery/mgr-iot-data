package pl.wat.mgr.witowski.mgriotdata.socket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.listener.SignDecryptListener;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

/**
 * @author: Piotr Witowski
 * @date: 25.12.2021
 * @project mgr-iot-data
 * Day of week: sobota
 */
@Log4j2
//@Component
public class WebSocketReceiver extends TextWebSocketHandler {

    private final ApplicationEventPublisher applicationEventPublisher;
    private final ObjectMapper objectMapper;
    private final DataType dataType;

    public WebSocketReceiver(ApplicationEventPublisher applicationEventPublisher, DataType dataType) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.dataType = dataType;
        objectMapper = new ObjectMapper();
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws JsonProcessingException {
//        log.info(objectMapper.writeValueAsString(message));
        BasicEvent event = EventFactory.builder()
                .agentUid(null)
                .topic(null)
                .code("iot-agent-000-000")
                .exceptionMessage(null)
                .inPayload(message.getPayload())
                .errors(Collections.EMPTY_LIST)
                .queueProcess(new LinkedList<>(Arrays.asList(SignDecryptListener.eventType)))
                .logType(LogEnum.DEBUG_LEVEL)
                .source(this)
                .start(LocalDateTime.now())
                .saveData(null)
                .eventList(new ArrayList<>())
                .dataType(dataType)
                .build()
                .createEvent();
        applicationEventPublisher.publishEvent(event);
    }


}
