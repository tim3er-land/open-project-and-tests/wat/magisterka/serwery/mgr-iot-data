package pl.wat.mgr.witowski.mgriotdata.service.validation;

import org.springframework.util.StringUtils;
import pl.wat.mgr.witowski.mgriotdata.commons.BasicValidationServices;
import pl.wat.mgr.witowski.mgriotdata.dto.SendDataDto;

public class SendDataToAgentValidation extends BasicValidationServices {
    private SendDataDto body;

    public SendDataToAgentValidation(String errorCode, String errorDescription, SendDataDto body) {
        super(errorCode, errorDescription);
        this.body = body;
    }

    @Override
    public void validate() {
        if (!StringUtils.hasText(body.getData())) {
            addNewErrorDto("agentUid", "agent-send-val-001-001", "agent-send-val-001-001");
        }
        if (!StringUtils.hasText(body.getTopic())) {
            addNewErrorDto("agentUid", "agent-send-val-001-002", "agent-send-val-001-002");
        }
        super.validate();
    }
}
