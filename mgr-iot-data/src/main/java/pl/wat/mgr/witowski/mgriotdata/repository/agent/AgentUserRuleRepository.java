package pl.wat.mgr.witowski.mgriotdata.repository.agent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentRuleEntity;

import java.util.List;

public interface AgentUserRuleRepository extends JpaRepository<IotAgentRuleEntity, String> {

    @Query("select ag from IotAgentRuleEntity ag where ag.iotUsrUid in (:usersUid)")
    List<IotAgentRuleEntity> getRuleForUser(@Param("usersUid") List<String> usersUid);
}
