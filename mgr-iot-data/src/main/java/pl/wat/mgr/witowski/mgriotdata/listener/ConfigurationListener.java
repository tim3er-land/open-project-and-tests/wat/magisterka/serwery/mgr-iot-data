package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentEntity;
import pl.wat.mgr.witowski.mgriotdata.dto.AgentConfigDto;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.event.PreperationEvent;
import pl.wat.mgr.witowski.mgriotdata.listener.interfaces.BasicDataListener;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentRepository;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentUserRuleRepository;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

@Log4j2
@Service
public class ConfigurationListener extends BasicDataListener<PreperationEvent> {
    public final static String eventType = "CONFIGURATION";
    private final AgentRepository agentRepository;
    private final AgentUserRuleRepository agentUserRuleRepository;
    private final RestTemplate restTemplate;
    private final String mgriotadministrationUrl;

    @Autowired
    public ConfigurationListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher
            , AgentRepository agentRepository, AgentUserRuleRepository agentUserRuleRepository
            , @Qualifier("dictioranyRestTemplare") RestTemplate restTemplate
            , @Value("${pl.wat.mgr.witowski.ms.mgriotadministration.url}") String mgriotadministrationUrl) {
        super(objectMapper, applicationEventPublisher);
        this.agentRepository = agentRepository;
        this.agentUserRuleRepository = agentUserRuleRepository;
        this.restTemplate = restTemplate;
        this.mgriotadministrationUrl = mgriotadministrationUrl;
    }

    @Override
    @EventListener(condition = "#data.nextStep.equalsIgnoreCase('CONFIGURATION')")
    public void reveiceData(PreperationEvent data) {
        BasicEvent event = null;
        try {
            IotAgentEntity agent = agentRepository.findByIotAgtUidJoinUser(data.getAgentUid());
            if (agent == null || CollectionUtils.isEmpty(agent.getIotAgentUsersByIotAgtId())) {
                event = EventFactory.builder()
                        .saveData(null)
                        .start(data.getStart())
                        .source(this)
                        .logType(LogEnum.WARN_LEVEL)
                        .queueProcess(new LinkedList<>(Arrays.asList(EndLogListener.nextStep)))
                        .agentUid(data.getAgentUid())
                        .errors(null)
                        .inPayload(data.getInPayload())
                        .topic(data.getTopic())
                        .exceptionMessage(null)
                        .code("iot-agent-003-001")
                        .messageUid(data.getMessageUid())
                        .endLogPayload(data.getPayload())
                        .eventList(addNewEventToList(eventType, data.getEventList()))
                        .dataType(data.getDataType())
                        .build().createEvent();
                applicationEventPublisher.publishEvent(event);
                return;
            }
            AgentConfigDto agentConfigDto = objectMapper.readValue(data.getPayload(), AgentConfigDto.class);
            processAgentConfig(agent, agentConfigDto);

            event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.INFO_LEVEL)
                    .queueProcess(data.getQueueProcess())
                    .saveData(extractPayload(data))
                    .agentUid(data.getAgentUid())
                    .topic(data.getTopic())
                    .errors(null)
                    .messageUid(agentConfigDto.getAgentUid())
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code("iot-agent-000-000")
                    .endLogPayload(data.getPayload())
                    .messageUid(data.getMessageUid())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
        } catch (Exception ex) {
            log.error("Error occured save agent config", ex);
            event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.INFO_LEVEL)
                    .queueProcess(data.getQueueProcess())
                    .saveData(extractPayload(data))
                    .agentUid(data.getAgentUid())
                    .topic(data.getTopic())
                    .errors(null)
                    .messageUid(data.getMessageUid())
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code("iot-agent-003-002")
                    .endLogPayload(data.getPayload())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .messageUid(data.getMessageUid())
                    .build()
                    .createEvent();
        }
        applicationEventPublisher.publishEvent(event);
    }

    private void processAgentConfig(IotAgentEntity agent, AgentConfigDto agentConfigDto) {
        restTemplate.delete(mgriotadministrationUrl + "/iot/agent/" + agent.getIotAgtUid() + "/topic");

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(mgriotadministrationUrl + "/iot/agent/" + agent.getIotAgtUid() + "/topic");
        for (String item : agentConfigDto.getTopic())
            builder.queryParam("topic", item);
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity =
                new HttpEntity<>(headers);

        restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.PUT,
                requestEntity, Void.class);
    }

    private String extractDataUid(String data) {
        try {
            HashMap hashMap = objectMapper.readValue(data, HashMap.class);
            if (hashMap.containsKey("messageUid")) {
                return objectMapper.writeValueAsString(hashMap.get("messageUid"));
            }
        } catch (Exception ex) {
            log.error("Error occured while parse data", ex);
        }
        return null;
    }

    private String extractTopic(String data) {
        try {
            HashMap hashMap = objectMapper.readValue(data, HashMap.class);
            if (hashMap.containsKey("topic")) {
                return (String) hashMap.get("topic");
            }
        } catch (Exception ex) {
            log.error("Error occured while parse data", ex);
        }
        return null;
    }

    private String extractPayload(PreperationEvent data) {
        try {
            HashMap hashMap = objectMapper.readValue(data.getPayload(), HashMap.class);
            if (hashMap.containsKey("payload")) {
                return objectMapper.writeValueAsString(hashMap.get("payload"));
            }
        } catch (Exception ex) {
            log.error("Error occured while parse data", ex);
        }
        return data.getPayload();
    }

}
