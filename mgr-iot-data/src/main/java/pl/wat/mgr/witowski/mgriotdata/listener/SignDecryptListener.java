package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentEntity;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.event.SignDecryptEvent;
import pl.wat.mgr.witowski.mgriotdata.listener.interfaces.BasicDataListener;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentRepository;
import pl.wat.mgr.witowski.mgriotdata.security.SecurityUtils;

import java.util.*;

import static pl.wat.mgr.witowski.mgriotdata.security.SecurityUtils.verify;

@Log4j2
@Service
public class SignDecryptListener extends BasicDataListener<SignDecryptEvent> {
    public final static String eventType = "SIGN_DECRYPT";

    private final AgentRepository agentRepository;
    private final String dataPrivDecript;

    public SignDecryptListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher, AgentRepository agentRepository
            , @Value("${pl.wat.mgr.witowski.ms.mgriotdata.key.decrypt.private}") String dataPrivDecript) {
        super(objectMapper, applicationEventPublisher);
        this.agentRepository = agentRepository;
        this.dataPrivDecript = dataPrivDecript;
    }

    @Override
    @EventListener(condition = "#data.nextStep.equalsIgnoreCase('SIGN_DECRYPT')")
    public void reveiceData(SignDecryptEvent data) {
        BasicEvent event = null;
        String payload = data.getInPayload();
        String[] splitPaylaod = payload.split("\\.");
        if (splitPaylaod.length != 3) {
            event = EventFactory.builder()
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.ERROR_LEVEL)
                    .queueProcess(new LinkedList<>(Arrays.asList(EndLogListener.nextStep)))
                    .agentUid(null)
                    .messageUid(data.getMessageUid())
                    .errors(null)
                    .inPayload(data.getInPayload())
                    .topic(data.getTopic())
                    .exceptionMessage(null)
                    .code("iot-agent-001-001")
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build().createEvent();
            applicationEventPublisher.publishEvent(event);
            return;
        }
        IotAgentEntity iotAgent = agentRepository.findByIotAgtUid(splitPaylaod[0]);

        boolean verify = false;
        try {
            verify = verify(iotAgent.getIotAgtPubKeySign(), splitPaylaod[1], splitPaylaod[2]);
        } catch (Exception ex) {
            log.error("Error occured while make sign ", ex);
            event = EventFactory.builder()
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.ERROR_LEVEL)
                    .queueProcess(new LinkedList<>(Arrays.asList(EndLogListener.nextStep)))
                    .agentUid(splitPaylaod[0])
                    .messageUid(data.getMessageUid())
                    .errors(null)
                    .inPayload(data.getInPayload())
                    .topic(data.getTopic())
                    .exceptionMessage(null)
                    .code("iot-agent-001-002")
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build().createEvent();
            applicationEventPublisher.publishEvent(event);
            return;
        }
        if (!verify) {
            event = EventFactory.builder()
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.ERROR_LEVEL)
                    .queueProcess(new LinkedList<>(Arrays.asList(EndLogListener.nextStep)))
                    .agentUid(splitPaylaod[0])
                    .errors(null)
                    .inPayload(data.getInPayload())
                    .topic(data.getTopic())
                    .exceptionMessage(null)
                    .messageUid(data.getMessageUid())
                    .code("iot-agent-001-003")
                    .endLogPayload(splitPaylaod[1])
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build().createEvent();
            applicationEventPublisher.publishEvent(event);
            return;
        }
        String payloadDecrypt = null;
        try {
            payloadDecrypt = SecurityUtils.decrypt(splitPaylaod[1], Base64.getDecoder().decode(dataPrivDecript));
        } catch (Exception ex) {
            log.error("Error occured while decrupt payload", ex);
            event = EventFactory.builder()
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.ERROR_LEVEL)
                    .queueProcess(new LinkedList<>(Arrays.asList(EndLogListener.nextStep)))
                    .agentUid(splitPaylaod[0])
                    .errors(null)
                    .inPayload(data.getInPayload())
                    .topic(data.getTopic())
                    .exceptionMessage(null)
                    .messageUid(data.getMessageUid())
                    .code("iot-agent-001-004")
                    .endLogPayload(splitPaylaod[1])
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build().createEvent();
            applicationEventPublisher.publishEvent(event);
            return;
        }
        event = EventFactory.builder()
                .saveData(payloadDecrypt)
                .start(data.getStart())
                .source(this)
                .logType(LogEnum.INFO_LEVEL)
                .queueProcess(new LinkedList<>(getEventQueueuAfterSighDecrypt(data.getDataType())))
                .agentUid(splitPaylaod[0])
                .errors(null)
                .inPayload(data.getInPayload())
                .topic(data.getTopic())
                .messageUid(extrackDataUid(payloadDecrypt))
                .exceptionMessage(null)
                .code("iot-agent-000-000")
                .endLogPayload(payloadDecrypt)
                .payload(payloadDecrypt)
                .agentUid(splitPaylaod[0])
                .eventList(addNewEventToList(eventType, data.getEventList()))
                .dataType(data.getDataType())
                .build().createEvent();
        applicationEventPublisher.publishEvent(event);
    }

    private String extrackDataUid(String payloadDecrypt) {
        try {
            HashMap hashMap = objectMapper.readValue(payloadDecrypt, HashMap.class);
            if (hashMap.containsKey("messageUid")) {
                return (String) hashMap.get("messageUid");
            }
        } catch (Exception ex) {
            log.error("Error occured while parse data", ex);
        }
        return null;
    }

    private List<String> getEventQueueuAfterSighDecrypt(DataType dataType) {
        if (dataType.equals(DataType.DATA))
            return Arrays.asList(PreparationListener.eventType);
        else
            return Arrays.asList(ConfigurationListener.eventType, StatusListener.eventType, EndLogListener.nextStep);
    }


}
