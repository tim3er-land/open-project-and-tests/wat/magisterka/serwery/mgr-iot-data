package pl.wat.mgr.witowski.mgriotdata.contrloller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.wat.mgr.witowski.mgriotdata.dao.AgentSummaryDto;
import pl.wat.mgr.witowski.mgriotdata.service.SummaryService;

@Log4j2
@CrossOrigin
@RestController
public class SummaryController {
    private final SummaryService summaryService;

    @Autowired
    public SummaryController(SummaryService summaryService) {
        this.summaryService = summaryService;
    }


    @GetMapping(path = "/iot/summary")
    public ResponseEntity<AgentSummaryDto> getIotSummary() {
        AgentSummaryDto agentSummaryDto = summaryService.getIotSummary();
        return ResponseEntity.ok(agentSummaryDto);
    }
}
