package pl.wat.mgr.witowski.mgriotdata.event;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Queue;


@Getter
@Setter
@JsonDeserialize(builder = PreperationEvent.PreperationEventBuilder.class)
public class PreperationEvent extends BasicEvent {
    private String payload;

    @Builder
    public PreperationEvent(Object source, Queue<String> queueProcess, String inPayload, LocalDateTime start, String nextStep, String topic, String agentUid, List<String> eventList, DataType dataType, String messageUid, List<String> sendAgentUid, String payload) {
        super(source, queueProcess, inPayload, start, nextStep, topic, agentUid, eventList, dataType, messageUid, sendAgentUid);
        this.payload = payload;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class PreperationEventBuilder extends BasicEvent.BasicEventBuilder {

    }
}