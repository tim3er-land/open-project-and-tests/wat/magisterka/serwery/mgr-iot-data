package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.dao.DataDao;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentEntity;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentRuleEntity;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentTopicEntity;
import pl.wat.mgr.witowski.mgriotdata.dto.WebSockerAgentSendDto;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.event.ProcessEvent;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentTopicRepository;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentUserRuleRepository;
import pl.wat.mgr.witowski.mgriotdata.service.SecurityService;
import pl.wat.mgr.witowski.mgriotdata.socket.WebSockerSender;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static pl.wat.mgr.witowski.mgriotdata.security.SecurityUtils.*;

@Log4j2
@Service
public class RedirectDataListener extends ProcessedListener<ProcessEvent> {

    public final static String eventType = "REDIRECT_DATA";
    private final AgentUserRuleRepository agentUserRuleRepository;
    private final AgentTopicRepository agentTopicRepository;
    private final WebSockerSender webSockerSender;
    private final SecurityService securityService;
    private final AmqpTemplate rabbitTemplate;

    @Autowired
    public RedirectDataListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher, AgentUserRuleRepository agentUserRuleRepository, AgentTopicRepository agentTopicRepository, WebSockerSender webSockerSender, SecurityService securityService, AmqpTemplate rabbitTemplate) {
        super(objectMapper, applicationEventPublisher);
        this.agentUserRuleRepository = agentUserRuleRepository;
        this.agentTopicRepository = agentTopicRepository;
        this.webSockerSender = webSockerSender;
        this.securityService = securityService;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    @EventListener(condition = "#data.nextStep.equalsIgnoreCase('REDIRECT_DATA')")
    public void reveiceData(ProcessEvent data) {
        List<WebSockerAgentSendDto> webSockerAgentSendDtos = new ArrayList<>();
        try {
            String processUid = data.getProcessUids().poll();
            Optional<IotAgentRuleEntity> ruleEntityOptional = agentUserRuleRepository.findById(processUid);
            if (!ruleEntityOptional.isPresent()) {
                BasicEvent event = EventFactory
                        .builder()
                        .saveData(null)
                        .start(data.getStart())
                        .source(this)
                        .logType(LogEnum.INFO_LEVEL)
                        .queueProcess(data.getQueueProcess())
                        .saveData(data.getSaveData())
                        .agentUid(data.getAgentUid())
                        .topic(data.getTopic())
                        .errors(null)
                        .messageUid(data.getMessageUid()).inPayload(data.getInPayload())
                        .exceptionMessage(null)
                        .code("iot-agent-004-001")
                        .endLogPayload(data.getSaveData())
                        .eventList(addNewEventToList(eventType, data.getEventList()))
                        .processUids(data.getProcessUids())
                        .dataType(data.getDataType())
                        .build()
                        .createEvent();
                applicationEventPublisher.publishEvent(event);
                return;
            }
            IotAgentRuleEntity ruleEntity = ruleEntityOptional.get();
            HashMap<String, String> ruleHashMap = objectMapper.readValue(ruleEntity.getIotAgtRulSetting(), HashMap.class);
            String rulSetting = ruleHashMap.get("config");
            rulSetting = prepareRule(rulSetting);
            if (!checkTopicValidation(data.getTopic(), extrackTopic(rulSetting))) {
                BasicEvent event = EventFactory
                        .builder()
                        .saveData(null)
                        .start(data.getStart())
                        .source(this)
                        .logType(LogEnum.INFO_LEVEL)
                        .queueProcess(data.getQueueProcess())
                        .saveData(data.getSaveData())
                        .agentUid(data.getAgentUid())
                        .topic(data.getTopic())
                        .errors(null)
                        .messageUid(data.getMessageUid()).inPayload(data.getInPayload())
                        .exceptionMessage(null)
                        .code("iot-agent-004-002")
                        .endLogPayload(data.getSaveData())
                        .eventList(addNewEventToList(eventType, data.getEventList()))
                        .processUids(data.getProcessUids())
                        .dataType(data.getDataType())
                        .build()
                        .createEvent();
                applicationEventPublisher.publishEvent(event);
                return;
            }
            DataDao processedDataDao = processed(rulSetting, data);
            if (processedDataDao != null) {
                List<IotAgentTopicEntity> userTopic = agentTopicRepository.getUserTopicByRule(ruleEntity.getIotAgtRulUid());
                webSockerAgentSendDtos = userTopic.stream()
                        .filter(iotAgentTopicEntity -> checkTopicValidation(processedDataDao.getTopic(),iotAgentTopicEntity.getIotAgtTopName()))
                        .map(iotAgentTopicEntity ->
                        {
                            try {
                                return WebSockerAgentSendDto.builder()
                                        .messageContent(prepareMessageContent(processedDataDao
                                                , iotAgentTopicEntity.getIotAgentByIotAgtId()))
                                        .agentName(iotAgentTopicEntity.getIotAgentByIotAgtId().getIotAgtUid())
                                        .sendDate(LocalDateTime.now().toString())
                                        .topic(processedDataDao.getTopic())
                                        .messageUid(UUID.randomUUID().toString())
                                        .build();
                            } catch (Exception ex) {
                                log.error("Error occured map WebSockerAgentSendDto", ex);
                                return null;
                            }

                        }).collect(Collectors.toList());
                webSockerAgentSendDtos.forEach(sendDto -> {
                    try {
                        rabbitTemplate.convertAndSend("mgr.direct","receive",objectMapper.writeValueAsString(sendDto));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                });

            }
            BasicEvent event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.INFO_LEVEL)
                    .queueProcess(data.getQueueProcess())
                    .saveData(data.getSaveData())
                    .agentUid(data.getAgentUid())
                    .topic(data.getTopic())
                    .errors(null)
                    .messageUid(data.getMessageUid())
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code("iot-agent-000-000")
                    .endLogPayload(data.getSaveData())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .processUids(data.getProcessUids())
                    .dataType(data.getDataType())
                    .sendAgentUid(webSockerAgentSendDtos.stream().map(WebSockerAgentSendDto::getAgentName).collect(Collectors.toList()))
                    .build()
                    .createEvent();
            applicationEventPublisher.publishEvent(event);
        } catch (
                Exception ex) {
            log.error("Error occured while REDIRECT_DATA", ex);
            BasicEvent event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.ERROR_LEVEL)
                    .queueProcess(data.getQueueProcess())
                    .saveData(data.getSaveData())
                    .agentUid(data.getAgentUid())
                    .topic(data.getTopic())
                    .errors(null)
                    .messageUid(data.getMessageUid())
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code("iot-agent-004-003")
                    .endLogPayload(data.getSaveData())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .processUids(data.getProcessUids())
                    .sendAgentUid(webSockerAgentSendDtos.stream().map(WebSockerAgentSendDto::getAgentName).collect(Collectors.toList()))
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
            applicationEventPublisher.publishEvent(event);
        }
    }

    private String prepareMessageContent(DataDao processedDataDao, IotAgentEntity iotAgtUid) throws Exception {
        String encrypt = encrypt(processedDataDao.getPayload(), Base64.getDecoder().decode(iotAgtUid.getIotAgtPubKeyDecr()));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(iotAgtUid.getIotAgtUid());
        stringBuilder.append(".");
        stringBuilder.append(encrypt);
        stringBuilder.append(".");
        stringBuilder.append(sign(encrypt, getPrivateKey(securityService.getSignPrivate())));
        return stringBuilder.toString();
    }

}
