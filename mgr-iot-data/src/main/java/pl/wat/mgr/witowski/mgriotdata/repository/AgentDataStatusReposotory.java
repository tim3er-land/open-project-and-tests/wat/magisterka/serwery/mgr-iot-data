package pl.wat.mgr.witowski.mgriotdata.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.mgr.witowski.mgriotdata.dao.AgentDataStatusDao;
import pl.wat.mgr.witowski.mgriotdata.dao.RawDataDao;

import java.util.List;

public interface AgentDataStatusReposotory extends MongoRepository<AgentDataStatusDao, String> {

    @Query("{ 'agentUid' : #{#agentUid} }")
    List<RawDataDao> getAgentDataRaw(@Param("agentUid") String agentUid, Pageable pageable);

    @Query("{ 'agentUid' :#{#agentUid}, 'topic' :#{#topicUid} }")
    List<RawDataDao> getAgentDataByTopicRaw(@Param("agentUid") String agentUid, @Param("topicUid") String topicUid, Pageable pageable);
}
