package pl.wat.mgr.witowski.mgriotdata.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.wat.mgr.witowski.mgriotdata.dto.FullKeyDto;
import pl.wat.mgr.witowski.mgriotdata.dto.MgrSecretDto;

import java.util.Collections;

@Log4j2
@Service
public class SecurityService {
    private final RestTemplate restTemplate;
    private final String mgrAuthUrl;
    private final String signPrivate;
    private final String signPublic;
    private final String decryptPrivate;
    private final String decryptPublic;

    public SecurityService(@Qualifier("dictioranyRestTemplare") RestTemplate restTemplate
            , @Value("${pl.wat.mgr.witowski.ms.mgrauth.url}") String mgrAuthUrl
            , @Value("${pl.wat.mgr.witowski.ms.mgriotdata.key.sign.private}") String signPrivate
            , @Value("${pl.wat.mgr.witowski.ms.mgriotdata.key.sign.public}") String signPublic
            , @Value("${pl.wat.mgr.witowski.ms.mgriotdata.key.decrypt.private}") String decryptPrivate
            , @Value("${pl.wat.mgr.witowski.ms.mgriotdata.key.decrypt.public}") String decryptPublic) {
        this.restTemplate = restTemplate;
        this.mgrAuthUrl = mgrAuthUrl;
        this.signPrivate = signPrivate;
        this.signPublic = signPublic;
        this.decryptPrivate = decryptPrivate;
        this.decryptPublic = decryptPublic;
    }

    public MgrSecretDto getMgrDataPubKey() {
        return MgrSecretDto.builder()
                .secretSign(signPublic)
                .secretDecrypt(decryptPublic)
                .build();
    }

    public String getSignPrivate() {
        return signPrivate;
    }

    public String getSignPublic() {
        return signPublic;
    }

    public String getDecryptPrivate() {
        return decryptPrivate;
    }

    public String getDecryptPublic() {
        return decryptPublic;
    }
}
