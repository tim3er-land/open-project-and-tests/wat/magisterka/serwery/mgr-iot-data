package pl.wat.mgr.witowski.mgriotdata.listener.interfaces;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.SubAgentLog;

import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

@Log4j2
@Service
public class BasicDataListener<T> implements BasicListener<T> {
    public final static String eventType = "basicData";
    protected final ObjectMapper objectMapper;
    protected final ApplicationEventPublisher applicationEventPublisher;

    public BasicDataListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher) {
        objectMapper.registerModule(new JavaTimeModule());
        this.objectMapper = objectMapper;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public void reveiceData(T data) {
        BasicEvent event = (BasicEvent) data;
        SubAgentLog.log(LogEnum.INFO_LEVEL, eventType, event.getInPayload()
                , event.getTopic(), event.getAgentUid(), event.getStart().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                , "200", Collections.emptyList(), Collections.emptyList(), DataType.DATA.getType(), null);
    }

    protected List<String> addNewEventToList(String eventName, List<String> list) {
        list.add(eventName);
        return list;
    }
}
