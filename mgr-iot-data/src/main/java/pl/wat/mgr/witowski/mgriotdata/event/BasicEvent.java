package pl.wat.mgr.witowski.mgriotdata.event;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Queue;

@Getter
@Setter
@JsonDeserialize(builder = BasicEvent.BasicEventBuilder.class)
public class BasicEvent extends ApplicationEvent {
    private Queue<String> queueProcess;
    private String inPayload;
    private LocalDateTime start;
    private String nextStep;
    private String topic;
    private String agentUid;
    private List<String> eventList;
    private DataType dataType;
    private String messageUid;
    private List<String> sendAgentUid;


    @Builder
    public BasicEvent(Object source, Queue<String> queueProcess, String inPayload, LocalDateTime start, String nextStep, String topic, String agentUid, List<String> eventList, DataType dataType, String messageUid, List<String> sendAgentUid) {
        super(source);
        this.queueProcess = queueProcess;
        this.inPayload = inPayload;
        this.start = start;
        this.nextStep = nextStep;
        this.topic = topic;
        this.agentUid = agentUid;
        this.eventList = eventList;
        this.dataType = dataType;
        this.messageUid = messageUid;
        this.sendAgentUid = sendAgentUid;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class BasicEventBuilder {

    }
}
