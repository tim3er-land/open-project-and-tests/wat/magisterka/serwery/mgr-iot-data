package pl.wat.mgr.witowski.mgriotdata.contrloller;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.wat.mgr.witowski.mgriotdata.dto.MgrSecretDto;
import pl.wat.mgr.witowski.mgriotdata.service.SecurityService;

@Log4j2
@CrossOrigin
@RestController
public class SecurityContlorer {

    private final SecurityService securityService;

    @Autowired
    public SecurityContlorer(SecurityService securityService) {
        this.securityService = securityService;
    }

    @GetMapping("/security/pub")//005
    @PreAuthorize("hasAnyRole('INNER_USER')")
    public ResponseEntity<MgrSecretDto> getDataPubKey() {
        MgrSecretDto fullKeyDto = securityService.getMgrDataPubKey();
        return new ResponseEntity<>(fullKeyDto, HttpStatus.OK);

    }
}
