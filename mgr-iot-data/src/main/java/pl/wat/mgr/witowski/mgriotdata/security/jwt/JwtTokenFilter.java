package pl.wat.mgr.witowski.mgriotdata.security.jwt;

import lombok.extern.log4j.Log4j2;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

@Log4j2
public class JwtTokenFilter extends OncePerRequestFilter {


    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String authToken = JwtUtils.resolveToken(req);
        if (authToken != null && JwtUtils.validateToken(authToken)) {
            String username = JwtUtils.getUsername(authToken);
            Authentication providerAuthentication = new UsernamePasswordAuthenticationToken(username, authToken, Collections.EMPTY_LIST);
            log.info("Authenticated user: {}, setting security context", username);
            SecurityContextHolder.getContext().setAuthentication(providerAuthentication);
        } else {
            log.warn("Request without jwt token");
        }
        chain.doFilter(req, res);
    }

}
