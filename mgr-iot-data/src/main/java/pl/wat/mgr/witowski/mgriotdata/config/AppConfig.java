package pl.wat.mgr.witowski.mgriotdata.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;
import pl.wat.mgr.witowski.mgriotdata.security.GenerateNewTokenService;
import pl.wat.mgr.witowski.mgriotdata.security.interceptor.AuthorizationInterceptor;

import javax.annotation.PostConstruct;
import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * @author: Piotr Witowski
 * @date: 25.12.2021
 * @project mgr-iot-data
 * Day of week: sobota
 */
@Log4j2
@Configuration
@EnableAsync
@Order(Ordered.HIGHEST_PRECEDENCE)
@EntityScan("pl.wat.mgr.witowski.mgriotdata")
@EnableMongoRepositories("pl.wat.mgr.witowski.mgriotdata.repository")
@ComponentScan({ "pl.wat.mgr.witowski.mgriotdata" })
public class AppConfig {

    @Autowired
    GenerateNewTokenService generateNewTokenService;

    @PostConstruct
    public void init() {
        try {
            log.debug("send Order To Generate NewKey ");
            generateNewTokenService.sendOrderToGenerateNewKey();
        } catch (Exception ex) {
            log.error("Errorr occured while generate key at startup ", ex);
        }
    }

    @Bean
    public RestTemplate restTemplate() {
        log.debug("Creating bean restTemplate");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                if (connection instanceof HttpsURLConnection) {
                    ((HttpsURLConnection) connection).setHostnameVerifier((s, sslSession) -> true);
                }
                super.prepareConnection(connection, httpMethod);
            }
        });
        return restTemplate;
    }

    @Bean("dictioranyRestTemplare")
    public RestTemplate dictioranyRestTemplare() {
        log.debug("creating bean dictioranyRestTemplare");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new AuthorizationInterceptor());
        return restTemplate;
    }

}
