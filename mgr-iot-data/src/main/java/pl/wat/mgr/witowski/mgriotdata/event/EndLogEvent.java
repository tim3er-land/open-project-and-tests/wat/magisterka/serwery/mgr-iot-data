package pl.wat.mgr.witowski.mgriotdata.event;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.wat.mgr.witowski.mgriotdata.commons.exceptions.dto.ErrorDto;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Queue;

@Getter
@Setter
@JsonDeserialize(builder = EndLogEvent.EndLogEventBuilder.class)
public class EndLogEvent extends BasicEvent {
    private String endLogPayload;
    private LogEnum logType;
    private String code;
    private String subCode;
    private List<ErrorDto> errors;
    private String exceptionMessage;

    @Builder
    public EndLogEvent(Object source, Queue<String> queueProcess, String inPayload, LocalDateTime start, String nextStep, String topic, String agentUid, List<String> eventList, DataType dataType, String messageUid, List<String> sendAgentUid, String endLogPayload, LogEnum logType, String code, String subCode, List<ErrorDto> errors, String exceptionMessage) {
        super(source, queueProcess, inPayload, start, nextStep, topic, agentUid, eventList, dataType, messageUid, sendAgentUid);
        this.endLogPayload = endLogPayload;
        this.logType = logType;
        this.code = code;
        this.subCode = subCode;
        this.errors = errors;
        this.exceptionMessage = exceptionMessage;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class EndLogEventBuilder extends BasicEvent.BasicEventBuilder {

    }
}
