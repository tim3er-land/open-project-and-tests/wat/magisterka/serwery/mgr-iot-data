package pl.wat.mgr.witowski.mgriotdata.queue;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.listener.SignDecryptListener;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

@Log4j2
@Service
public class QueueListener {
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public QueueListener(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }


    @RabbitListener(queues = {"dataQueue"})
    public void receiveData(@Payload String fileBody) {
        System.out.println("Message data " + fileBody);
        BasicEvent event = EventFactory.builder()
                .agentUid(null)
                .topic(null)
                .code("iot-agent-000-000")
                .exceptionMessage(null)
                .inPayload(fileBody)
                .errors(Collections.EMPTY_LIST)
                .queueProcess(new LinkedList<>(Arrays.asList(SignDecryptListener.eventType)))
                .logType(LogEnum.DEBUG_LEVEL)
                .source(this)
                .start(LocalDateTime.now())
                .saveData(null)
                .eventList(new ArrayList<>())
                .dataType(DataType.DATA)
                .build()
                .createEvent();
        applicationEventPublisher.publishEvent(event);
    }

    @RabbitListener(queues = {"configQueue"})
    public void receiveConfig(@Payload String fileBody) {
        System.out.println("Message config " + fileBody);
        BasicEvent event = EventFactory.builder()
                .agentUid(null)
                .topic(null)
                .code("iot-agent-000-000")
                .exceptionMessage(null)
                .inPayload(fileBody)
                .errors(Collections.EMPTY_LIST)
                .queueProcess(new LinkedList<>(Arrays.asList(SignDecryptListener.eventType)))
                .logType(LogEnum.DEBUG_LEVEL)
                .source(this)
                .start(LocalDateTime.now())
                .saveData(null)
                .eventList(new ArrayList<>())
                .dataType(DataType.CONFIG)
                .build()
                .createEvent();
        applicationEventPublisher.publishEvent(event);
    }
}
