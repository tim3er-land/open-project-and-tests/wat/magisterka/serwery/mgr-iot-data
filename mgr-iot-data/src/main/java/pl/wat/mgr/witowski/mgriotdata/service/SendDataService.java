package pl.wat.mgr.witowski.mgriotdata.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentEntity;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentTopicEntity;
import pl.wat.mgr.witowski.mgriotdata.dto.SendDataDto;
import pl.wat.mgr.witowski.mgriotdata.dto.WebSockerAgentSendDto;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentRepository;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentTopicRepository;
import pl.wat.mgr.witowski.mgriotdata.security.jwt.JwtUtils;
import pl.wat.mgr.witowski.mgriotdata.service.validation.SendDataToAgentValidation;
import pl.wat.mgr.witowski.mgriotdata.socket.WebSockerSender;

import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static pl.wat.mgr.witowski.mgriotdata.security.SecurityUtils.*;

@Log4j2
@Service
public class SendDataService {

    private final AgentRepository agentRepository;
    private final AgentTopicRepository agentTopicRepository;
    private final WebSockerSender webSockerSender;
    private final SecurityService securityService;
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public SendDataService(AgentRepository agentRepository, AgentTopicRepository agentTopicRepository, WebSockerSender webSockerSender, SecurityService securityService, RabbitTemplate rabbitTemplate) {
        this.agentRepository = agentRepository;
        this.agentTopicRepository = agentTopicRepository;
        this.webSockerSender = webSockerSender;
        this.securityService = securityService;
        this.rabbitTemplate = rabbitTemplate;
        objectMapper = new ObjectMapper();
    }

    public void sendDataToAgent(SendDataDto body) {
        try {
            new SendDataToAgentValidation("mgr-iot-data-005-002", "mgr-iot-data-005-002", body).validate();
            List<WebSockerAgentSendDto> webSockerAgentSendDtos = new ArrayList<>();
            List<IotAgentTopicEntity> userTopic = agentTopicRepository.getUserTopicByUser(JwtUtils.getUUID());
            webSockerAgentSendDtos = userTopic.stream()
                    .filter(iotAgentTopicEntity -> checkTopicValidation(body.getTopic(), iotAgentTopicEntity.getIotAgtTopName()))
                    .map(iotAgentTopicEntity ->
                    {
                        try {
                            return WebSockerAgentSendDto.builder()
                                    .messageContent(prepareMessageContent(getBody(body)
                                            , iotAgentTopicEntity.getIotAgentByIotAgtId()))
                                    .agentName(iotAgentTopicEntity.getIotAgentByIotAgtId().getIotAgtUid())
                                    .sendDate(LocalDateTime.now().toString())
                                    .topic(body.getTopic())
                                    .messageUid(UUID.randomUUID().toString())
                                    .build();
                        } catch (Exception ex) {
                            log.error("Error occured map WebSockerAgentSendDto", ex);
                            return null;
                        }

                    }).collect(Collectors.toList());
            webSockerAgentSendDtos.forEach(sendDto -> {
                try {
                    rabbitTemplate.convertAndSend("mgr.direct", "receive", objectMapper.writeValueAsString(sendDto));
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

            });
        } catch (ApiExceptions ae) {
            throw ae;
        } catch (Exception ex) {
            log.error("Error occured while sendDataToAgent", ex);
            throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-data-005-003", "mgr-iot-data-005-003");
        }
    }

    private String getBody(SendDataDto body) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Map map = objectMapper.readValue(body.getData(), Map.class);
            return body.getData();
        } catch (Exception ex) {
            HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
            objectObjectHashMap.put("payload", body.getData());
            return objectMapper.writeValueAsString(objectObjectHashMap);
        }
    }

    protected boolean checkTopicValidation(String topicData, String topic) {
        String topicExp = topic.trim()
                .replaceAll("\\$", "\\\\\\$")
                .replaceAll("\\+", "[^/]+")
                .replaceAll("/\\#$", "(\\$|/.+)");

        Pattern pattern = Pattern.compile(topicExp);
        boolean match = pattern.matcher(topicData).matches();
        return match;
    }

    private String prepareMessageContent(String payload, IotAgentEntity iotAgtUid) throws Exception {
        String encrypt = encrypt(payload, Base64.getDecoder().decode(iotAgtUid.getIotAgtPubKeyDecr()));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(iotAgtUid.getIotAgtUid());
        stringBuilder.append(".");
        stringBuilder.append(encrypt);
        stringBuilder.append(".");
        stringBuilder.append(sign(encrypt, getPrivateKey(securityService.getSignPrivate())));
        return stringBuilder.toString();
    }

}
