package pl.wat.mgr.witowski.mgriotdata.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentRawDataDto {
    private String creationDate;
    private String topic;
    private HashMap<String,Object> payload;
    private String agentUid;


}
