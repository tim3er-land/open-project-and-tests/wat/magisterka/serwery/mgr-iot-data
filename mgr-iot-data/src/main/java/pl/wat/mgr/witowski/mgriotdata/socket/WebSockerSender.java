package pl.wat.mgr.witowski.mgriotdata.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.mgriotdata.dto.WebSockerAgentSendDto;

import java.net.URI;

/**
 * @author: Piotr Witowski
 * @date: 25.12.2021
 * @project mgr-iot-data
 * Day of week: sobota
 */

@Log4j2
@Service
public class WebSockerSender {
    private final String addres;
    private final ObjectMapper objectMapper;

    public WebSockerSender(@Value("${webSocketClientAdress}") String addres) {
        this.addres = addres;
        objectMapper = new ObjectMapper();
    }

    public void sendData(WebSockerAgentSendDto sendDto) {
        WebSocketClient webSocketClient = new WebSocketClient(URI.create(addres)) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                log.info("open conncection with status: {}", serverHandshake.getHttpStatusMessage());
            }

            @Override
            public void onMessage(String s) {
                log.info("send data: {}", s);
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                log.info("Close conncection with status: {}", i);
            }

            @Override
            public void onError(Exception e) {
                log.error("Error occured while use Web socker", e);
            }
        };
        try {
            webSocketClient.connectBlocking();
            webSocketClient.send(objectMapper.writeValueAsString(sendDto));
        } catch (Exception ex) {
            log.error("Error occured while sedn data throw webSocket", ex);
            throw new ApiExceptions(HttpStatus.BAD_REQUEST, "mgr-iot-data-000-001", "mgr-iot-data-000-001");
        } finally {
            webSocketClient.close();
        }
    }

}
