package pl.wat.mgr.witowski.mgriotdata.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentSummaryDto {
    private Integer agentNumber;
    private Integer ruleNumber;
    private Integer lastHourData;
    private Integer lastDayData;
    private Integer dataNumber;
    private Map<String,Integer> topicNumber;
}
