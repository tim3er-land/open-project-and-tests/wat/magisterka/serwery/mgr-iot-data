package pl.wat.mgr.witowski.mgriotdata.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.wat.mgr.witowski.mgriotdata.enums.ConditionSigh;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WhereDto {
    private String name;
    private String condition;
    private String value;
    private ConditionSigh conditionSigh;
}
