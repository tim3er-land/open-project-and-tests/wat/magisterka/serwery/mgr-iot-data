package pl.wat.mgr.witowski.mgriotdata.logs.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import pl.wat.mgr.witowski.mgriotdata.logs.dto.SubLogDto;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum.*;

/**
 * @author: Piotr Witowski
 * @date: 04.02.2022
 * @project tim3erland-iot
 * Day of week: piątek
 */
@Log4j2
public class SubAgentLog {
    private static ObjectMapper mapper;

    public static void log(LogEnum logLevel, String op, String payload, String topic, String deviceUid, Long start
            , String status, List<String> eventList, List<String> sendAgentUid, String type, String exceptionMessage) {
        HashMap<String, Object> map = null;
        if (mapper == null) {
            mapper = new ObjectMapper();
        }
        try {
            map = mapper.readValue(payload, HashMap.class);
        } catch (Exception ex) {
            log.error("Error occured while convert json to map ", ex.getMessage());
            map = new HashMap<>();
            map.put("payload", payload);
        }
        log(logLevel, op, map, topic, deviceUid, start, status, eventList, sendAgentUid, type, exceptionMessage);

    }

    public static void log(LogEnum logLevel, String op, HashMap<String, Object> payload
            , String topic, String deviceUid, Long start, String status
            , List<String> eventList, List<String> sendAgentUid, String type
            , String exceptionMessage) {
        SubLogDto subLogDto = SubLogDto.builder()
                .dur(calculateDur(start))
                .op(op)
                .deviceUid(deviceUid)
                .topic(topic)
                .logStatus(status)
                .payload(payload)
                .eventList(eventList)
                .dataType(type)
                .sendAgentUid(sendAgentUid)
                .exceptionMessage(exceptionMessage)
                .build();


        if (mapper == null) {
            mapper = new ObjectMapper();
        }
        try {
            if (logLevel.equals(INFO_LEVEL)) {
                log.info(mapper.writeValueAsString(subLogDto));
            } else if (logLevel.equals(WARN_LEVEL)) {
                log.warn(mapper.writeValueAsString(subLogDto));
            } else if (logLevel.equals(ERROR_LEVEL)) {
                log.error(mapper.writeValueAsString(subLogDto));
            } else {
                log.debug(mapper.writeValueAsString(subLogDto));
            }
        } catch (
                Exception ex) {
            log.error("Error occured while map data to log", ex);
        }

    }

    private static Long calculateDur(Long start) {
        return Calendar.getInstance().getTime().getTime() - start;
    }
}
