package pl.wat.mgr.witowski.mgriotdata.event;

import lombok.Builder;
import pl.wat.mgr.witowski.mgriotdata.commons.exceptions.dto.ErrorDto;
import pl.wat.mgr.witowski.mgriotdata.enums.DataType;
import pl.wat.mgr.witowski.mgriotdata.listener.*;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

/**
 * @author: Piotr Witowski
 * @date: 04.02.2022
 * @project tim3erland-iot
 * Day of week: piątek
 */
public class EventFactory {

    private Queue<String> queueProcess;
    private String inPayload;
    private LocalDateTime start;
    private String agentUid;
    private LogEnum logType;
    private String code;
    private List<ErrorDto> errors;
    private String exceptionMessage;
    private String saveData;
    private Object source;
    private String topic;
    private String endLogPayload;
    private String payload;
    private String messageUid;
    private Queue<String> processUids;
    private List<String> eventList;
    private DataType dataType;
    private List<String> sendAgentUid;

    @Builder
    public EventFactory(Queue<String> queueProcess, String inPayload, LocalDateTime start, String agentUid, LogEnum logType, String code, List<ErrorDto> errors, String exceptionMessage, String saveData, Object source, String topic, String endLogPayload, String payload, String messageUid, Queue<String> processUids, List<String> eventList, DataType dataType, List<String> sendAgentUid) {
        this.queueProcess = queueProcess;
        this.inPayload = inPayload;
        this.start = start;
        this.agentUid = agentUid;
        this.logType = logType;
        this.code = code;
        this.errors = errors;
        this.exceptionMessage = exceptionMessage;
        this.saveData = saveData;
        this.source = source;
        this.topic = topic;
        this.endLogPayload = endLogPayload;
        this.payload = payload;
        this.messageUid = messageUid;
        this.processUids = processUids;
        this.eventList = eventList;
        this.dataType = dataType;
        this.sendAgentUid = sendAgentUid;
    }

    public BasicEvent createEvent() {
        String event = this.queueProcess.poll();
        switch (event.toUpperCase(Locale.ROOT)) {
            case EndLogListener.nextStep:
                return EndLogEvent.builder()
                        .start(this.start)
                        .code(this.code)
                        .agentUid(this.agentUid)
                        .inPayload(this.inPayload)
                        .queueProcess(this.queueProcess)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .logType(this.logType)
                        .errors(this.errors)
                        .exceptionMessage(this.exceptionMessage)
                        .endLogPayload(this.endLogPayload)
                        .eventList(eventList)
                        .topic(topic)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)
                        .build();
            case StatusListener.eventType:
                return EndLogEvent.builder()
                        .start(this.start)
                        .code(this.code)
                        .agentUid(this.agentUid)
                        .inPayload(this.inPayload)
                        .queueProcess(this.queueProcess)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .logType(this.logType)
                        .errors(this.errors)
                        .exceptionMessage(this.exceptionMessage)
                        .endLogPayload(this.endLogPayload)
                        .eventList(eventList)
                        .topic(topic)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)
                        .build();

            case SignDecryptListener.eventType:
                return SignDecryptEvent.signDecryptEventBuilder()
                        .queueProcess(queueProcess)
                        .start(start)
                        .agentUid(agentUid)
                        .inPayload(inPayload)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .topic(topic)
                        .eventList(eventList)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)
                        .build();
            case PreparationListener.eventType:
                return PreperationEvent.builder()
                        .start(this.start)
                        .agentUid(this.agentUid)
                        .inPayload(this.inPayload)
                        .queueProcess(this.queueProcess)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .payload(this.payload)
                        .agentUid(agentUid)
                        .eventList(eventList)
                        .topic(topic)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)
                        .build();
            case ConfigurationListener.eventType:
                return PreperationEvent.builder()
                        .start(this.start)
                        .agentUid(this.agentUid)
                        .inPayload(this.inPayload)
                        .queueProcess(this.queueProcess)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .payload(this.payload)
                        .agentUid(agentUid)
                        .eventList(eventList)
                        .topic(topic)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)

                        .build();

//             Procesory
            case SaveDataListener.eventType:
                return ProcessEvent.saveDataEventBuilder()
                        .start(this.start)
                        .agentUid(this.agentUid)
                        .inPayload(this.inPayload)
                        .queueProcess(this.queueProcess)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .saveData(this.saveData)
                        .agentUid(agentUid)
                        .processUids(processUids)
                        .topic(topic)
                        .eventList(eventList)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)

                        .build();
            case SaveRawDataListener.eventType:
                return ProcessEvent.saveDataEventBuilder()
                        .start(this.start)
                        .agentUid(this.agentUid)
                        .inPayload(this.inPayload)
                        .queueProcess(this.queueProcess)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .saveData(this.saveData)
                        .agentUid(agentUid)
                        .topic(topic)
                        .processUids(processUids)
                        .eventList(eventList)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)

                        .build();
            case RedirectDataListener.eventType:
                return ProcessEvent.saveDataEventBuilder()
                        .start(this.start)
                        .agentUid(this.agentUid)
                        .inPayload(this.inPayload)
                        .queueProcess(this.queueProcess)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .saveData(this.saveData)
                        .agentUid(agentUid)
                        .topic(topic)
                        .processUids(processUids)
                        .eventList(eventList)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)

                        .build();
            default:
                return BasicEvent.builder()
                        .start(start)
                        .agentUid(agentUid)
                        .inPayload(inPayload)
                        .topic(topic)
                        .queueProcess(queueProcess)
                        .nextStep(event.toUpperCase(Locale.ROOT))
                        .source(source)
                        .eventList(eventList)
                        .dataType(dataType)
                        .sendAgentUid(sendAgentUid)

                        .build();
        }
    }
}
