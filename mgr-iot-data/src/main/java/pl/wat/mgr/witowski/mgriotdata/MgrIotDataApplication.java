package pl.wat.mgr.witowski.mgriotdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgrIotDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(MgrIotDataApplication.class, args);
    }

}
