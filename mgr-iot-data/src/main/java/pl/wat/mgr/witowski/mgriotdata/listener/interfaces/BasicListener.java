package pl.wat.mgr.witowski.mgriotdata.listener.interfaces;

/**
 * @author: Piotr Witowski
 * @date: 11.10.2021
 * @project tim3erland-iot
 * Day of week: poniedziałek
 */
public interface BasicListener<T> {
    public void reveiceData(T data);
}
