package pl.wat.mgr.witowski.mgriotdata.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentDataDto {
    private String messageUid;
    private String messageDate;
    private HashMap<String, Object> data;
}
