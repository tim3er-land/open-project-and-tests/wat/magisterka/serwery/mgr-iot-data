package pl.wat.mgr.witowski.mgriotdata.dao.agent;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "iot_agent_rule", schema = "public", catalog = "postgres_mgr")
public class IotAgentRuleEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "iot_agt_rul_uid")
    private String iotAgtRulUid;
    @Basic
    @Column(name = "iot_agt_rul_act")
    private short iotAgtRulAct;
    @Basic
    @Column(name = "iot_usr_uid")
    private String iotUsrUid;
    @Basic
    @Column(name = "iot_agt_rul_code")
    private String iotAgtRulCode;
    @Basic
    @Column(name = "iot_agt_rul_setting")
    private String iotAgtRulSetting;
    @Basic
    @Column(name = "iot_agt_rul_insert_by")
    private String iotAgtRulInsertBy;
    @Basic
    @Column(name = "iot_agt_rul_modify_by")
    private String iotAgtRulModifyBy;
    @Basic
    @Column(name = "iot_agt_rul_insert_date")
    private Timestamp iotAgtRulInsertDate;
    @Basic
    @Column(name = "iot_agt_rul_modify_date")
    private Timestamp iotAgtRulModifyDate;

    public String getIotAgtRulUid() {
        return iotAgtRulUid;
    }

    public void setIotAgtRulUid(String iotAgtRulUid) {
        this.iotAgtRulUid = iotAgtRulUid;
    }

    public short getIotAgtRulAct() {
        return iotAgtRulAct;
    }

    public void setIotAgtRulAct(short iotAgtRulAct) {
        this.iotAgtRulAct = iotAgtRulAct;
    }

    public String getIotUsrUid() {
        return iotUsrUid;
    }

    public void setIotUsrUid(String iotUsrUid) {
        this.iotUsrUid = iotUsrUid;
    }

    public String getIotAgtRulCode() {
        return iotAgtRulCode;
    }

    public void setIotAgtRulCode(String iotAgtRulCode) {
        this.iotAgtRulCode = iotAgtRulCode;
    }

    public String getIotAgtRulSetting() {
        return iotAgtRulSetting;
    }

    public void setIotAgtRulSetting(String iotAgtRulSetting) {
        this.iotAgtRulSetting = iotAgtRulSetting;
    }

    public String getIotAgtRulInsertBy() {
        return iotAgtRulInsertBy;
    }

    public void setIotAgtRulInsertBy(String iotAgtRulInsertBy) {
        this.iotAgtRulInsertBy = iotAgtRulInsertBy;
    }

    public String getIotAgtRulModifyBy() {
        return iotAgtRulModifyBy;
    }

    public void setIotAgtRulModifyBy(String iotAgtRulModifyBy) {
        this.iotAgtRulModifyBy = iotAgtRulModifyBy;
    }

    public Timestamp getIotAgtRulInsertDate() {
        return iotAgtRulInsertDate;
    }

    public void setIotAgtRulInsertDate(Timestamp iotAgtRulInsertDate) {
        this.iotAgtRulInsertDate = iotAgtRulInsertDate;
    }

    public Timestamp getIotAgtRulModifyDate() {
        return iotAgtRulModifyDate;
    }

    public void setIotAgtRulModifyDate(Timestamp iotAgtRulModifyDate) {
        this.iotAgtRulModifyDate = iotAgtRulModifyDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IotAgentRuleEntity that = (IotAgentRuleEntity) o;
        return iotAgtRulAct == that.iotAgtRulAct && Objects.equals(iotAgtRulUid, that.iotAgtRulUid) && Objects.equals(iotUsrUid, that.iotUsrUid) && Objects.equals(iotAgtRulCode, that.iotAgtRulCode) && Objects.equals(iotAgtRulSetting, that.iotAgtRulSetting) && Objects.equals(iotAgtRulInsertBy, that.iotAgtRulInsertBy) && Objects.equals(iotAgtRulModifyBy, that.iotAgtRulModifyBy) && Objects.equals(iotAgtRulInsertDate, that.iotAgtRulInsertDate) && Objects.equals(iotAgtRulModifyDate, that.iotAgtRulModifyDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iotAgtRulUid, iotAgtRulAct, iotUsrUid, iotAgtRulCode, iotAgtRulSetting, iotAgtRulInsertBy, iotAgtRulModifyBy, iotAgtRulInsertDate, iotAgtRulModifyDate);
    }
}
