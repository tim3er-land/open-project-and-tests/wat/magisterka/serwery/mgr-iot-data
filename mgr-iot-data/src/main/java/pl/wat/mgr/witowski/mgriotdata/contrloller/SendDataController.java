package pl.wat.mgr.witowski.mgriotdata.contrloller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.wat.mgr.witowski.mgriotdata.dto.SendDataDto;
import pl.wat.mgr.witowski.mgriotdata.service.SendDataService;

@Log4j2
@CrossOrigin
@RestController
public class SendDataController {

    private final SendDataService sendDataService;

    @Autowired
    public SendDataController(SendDataService sendDataService) {
        this.sendDataService = sendDataService;
    }

    @PutMapping("/agent/send")
    public void sendDataToAgent(@RequestBody SendDataDto sendDataDto) {
        sendDataService.sendDataToAgent(sendDataDto);
    }
}
