package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.dao.DataDao;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentRuleEntity;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.event.ProcessEvent;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;
import pl.wat.mgr.witowski.mgriotdata.repository.DataReposotory;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentUserRuleRepository;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@Service
//listener do zapisy przetworzonych danych
public class SaveDataListener extends ProcessedListener<ProcessEvent> {
    public final static String eventType = "SAVE_PROCESSED_DATA";
    private final AgentUserRuleRepository agentUserRuleRepository;
    private final DataReposotory dataReposotory;

    @Autowired
    public SaveDataListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher, AgentUserRuleRepository agentUserRuleRepository, DataReposotory dataReposotory) {
        super(objectMapper, applicationEventPublisher);
        this.agentUserRuleRepository = agentUserRuleRepository;
        this.dataReposotory = dataReposotory;
    }

    @Override
    @EventListener(condition = "#data.nextStep.equalsIgnoreCase('SAVE_PROCESSED_DATA')")
    public void reveiceData(ProcessEvent data) {
        try {
            String processUid = data.getProcessUids().poll();
            Optional<IotAgentRuleEntity> ruleEntityOptional = agentUserRuleRepository.findById(processUid);
            if (!ruleEntityOptional.isPresent()) {
                BasicEvent event = EventFactory
                        .builder()
                        .saveData(null)
                        .start(data.getStart())
                        .source(this)
                        .logType(LogEnum.INFO_LEVEL)
                        .queueProcess(data.getQueueProcess())
                        .saveData(data.getSaveData())
                        .agentUid(data.getAgentUid())
                        .topic(data.getTopic())
                        .errors(null)
                        .messageUid(data.getMessageUid())
                        .inPayload(data.getInPayload())
                        .exceptionMessage(null)
                        .code("iot-agent-005-001")
                        .endLogPayload(data.getSaveData())
                        .eventList(addNewEventToList(eventType, data.getEventList()))
                        .processUids(data.getProcessUids())
                        .dataType(data.getDataType())
                        .build()
                        .createEvent();
                applicationEventPublisher.publishEvent(event);
                return;
            }
            IotAgentRuleEntity ruleEntity = ruleEntityOptional.get();
            HashMap<String, String> ruleHashMap = objectMapper.readValue(ruleEntity.getIotAgtRulSetting(), HashMap.class);
            String rulSetting = ruleHashMap.get("config");
            rulSetting = prepareRule(rulSetting);
            if (!checkTopicValidation(data.getTopic(), extrackTopic(rulSetting))) {
                BasicEvent event = EventFactory
                        .builder()
                        .saveData(null)
                        .start(data.getStart())
                        .source(this)
                        .logType(LogEnum.WARN_LEVEL)
                        .queueProcess(data.getQueueProcess())
                        .saveData(data.getSaveData())
                        .agentUid(data.getAgentUid())
                        .topic(data.getTopic())
                        .errors(null)
                        .messageUid(data.getMessageUid())
                        .inPayload(data.getInPayload())
                        .exceptionMessage(null)
                        .code("iot-agent-005-002")
                        .endLogPayload(data.getSaveData())
                        .eventList(addNewEventToList(eventType, data.getEventList()))
                        .processUids(data.getProcessUids())
                        .dataType(data.getDataType())
                        .build()
                        .createEvent();
                applicationEventPublisher.publishEvent(event);
                return;
            }
            DataDao processedDataDao = processed(rulSetting, data);
            if (processedDataDao != null) {
                processedDataDao.setDataUid(UUID.randomUUID().toString());
                processedDataDao.setAgentUid(data.getAgentUid());
                processedDataDao.setTopic(extrackTopic(rulSetting));
                processedDataDao.setCreationDate(LocalDateTime.now());
                dataReposotory.save(processedDataDao);
            }
            BasicEvent event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.INFO_LEVEL)
                    .queueProcess(data.getQueueProcess())
                    .saveData(data.getSaveData())
                    .agentUid(data.getAgentUid())
                    .topic(data.getTopic())
                    .errors(null)
                    .messageUid(data.getMessageUid())
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code("iot-agent-000-000")
                    .endLogPayload(data.getSaveData())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .processUids(data.getProcessUids())
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
            applicationEventPublisher.publishEvent(event);
        } catch (Exception ex) {
            log.error("Error occured while SAVE_PROCESSED_DATA", ex);
            BasicEvent event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.ERROR_LEVEL)
                    .queueProcess(data.getQueueProcess())
                    .saveData(data.getSaveData())
                    .agentUid(data.getAgentUid())
                    .topic(data.getTopic())
                    .errors(null)
                    .messageUid(data.getMessageUid())
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code("iot-agent-005-003")
                    .endLogPayload(data.getSaveData())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .processUids(data.getProcessUids())
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
            applicationEventPublisher.publishEvent(event);
        }
    }

}
