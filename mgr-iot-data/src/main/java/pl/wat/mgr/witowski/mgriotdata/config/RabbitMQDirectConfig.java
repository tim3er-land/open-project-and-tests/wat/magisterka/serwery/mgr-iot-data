package pl.wat.mgr.witowski.mgriotdata.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQDirectConfig {

    @Bean
    Queue marketingQueue() {
        return new Queue("dataQueue", false);
    }

    @Bean
    Queue financeQueue() {
        return new Queue("configQueue", false);
    }

    @Bean
    Queue adminQueue() {
        return new Queue("receiveQueue", false);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange("mgr.direct");
    }

    @Bean
    Binding marketingBinding(Queue marketingQueue, DirectExchange exchange) {
        return BindingBuilder.bind(marketingQueue).to(exchange).with("data");
    }

    @Bean
    Binding financeBinding(Queue financeQueue, DirectExchange exchange) {
        return BindingBuilder.bind(financeQueue).to(exchange).with("config");
    }

    @Bean
    Binding adminBinding(Queue adminQueue, DirectExchange exchange) {
        return BindingBuilder.bind(adminQueue).to(exchange).with("receive");
    }
}
