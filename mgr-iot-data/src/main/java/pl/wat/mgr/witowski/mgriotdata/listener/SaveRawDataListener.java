package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.dao.RawDataDao;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.event.ProcessEvent;
import pl.wat.mgr.witowski.mgriotdata.listener.interfaces.BasicDataListener;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;
import pl.wat.mgr.witowski.mgriotdata.repository.RawDataReposotory;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author: Piotr Witowski
 * @date: 10.10.2021
 * @project tim3erland-iot
 * Day of week: niedziela
 */

@Log4j2
@Service
public class SaveRawDataListener extends BasicDataListener<ProcessEvent> {
    public final static String eventType = "SAVE_RAW_DATA";

    private final ApplicationEventPublisher applicationEventPublisher;
    private final RawDataReposotory deviceDataRepository;

    @Autowired
    public SaveRawDataListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher, ApplicationEventPublisher applicationEventPublisher1, RawDataReposotory deviceDataRepository) {
        super(objectMapper, applicationEventPublisher);
        this.applicationEventPublisher = applicationEventPublisher1;
        this.deviceDataRepository = deviceDataRepository;
    }

    @Override
    @EventListener(condition = "#data.nextStep.equalsIgnoreCase('SAVE_RAW_DATA')")
    public void reveiceData(ProcessEvent data) {
        BasicEvent event = null;
        try {
            data.getProcessUids().poll();
            RawDataDao sensorsDataDao = RawDataDao.builder()
                    .agentUid(data.getAgentUid())
                    .dataUid(UUID.randomUUID().toString())
                    .creationDate(LocalDateTime.now())
                    .payload(data.getSaveData())
                    .topic(data.getTopic())
                    .build();
            deviceDataRepository.save(sensorsDataDao);
            event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .agentUid(data.getAgentUid())
                    .logType(LogEnum.INFO_LEVEL)
                    .queueProcess(data.getQueueProcess())
                    .messageUid(data.getMessageUid())
                    .saveData(data.getSaveData())
                    .agentUid(data.getAgentUid())
                    .errors(null)
                    .inPayload(data.getInPayload())
                    .topic(data.getTopic())
                    .exceptionMessage(null)
                    .code("iot-agent-000-000")
                    .endLogPayload(data.getSaveData())
                    .eventList(addNewEventToList(eventType,data.getEventList()))
                    .processUids(data.getProcessUids())
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
        } catch (Exception ex) {
            log.error("Error occured prepare and save device data to db", ex);
            event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.INFO_LEVEL)
                    .queueProcess(data.getQueueProcess())
                    .saveData(data.getSaveData())
                    .agentUid(data.getAgentUid())
                    .errors(null)
                    .inPayload(data.getInPayload())
                    .topic(data.getTopic())
                    .exceptionMessage(null)
                    .code("iot-agent-006-001")
                    .messageUid(data.getMessageUid())
                    .endLogPayload(data.getSaveData())
                    .exceptionMessage(ex.getMessage())
                    .eventList(addNewEventToList(eventType,data.getEventList()))
                    .processUids(data.getProcessUids())
                    .dataType(data.getDataType())
                    .sendAgentUid(data.getSendAgentUid())
                    .build()
                    .createEvent();
        }
        applicationEventPublisher.publishEvent(event);
    }


}
