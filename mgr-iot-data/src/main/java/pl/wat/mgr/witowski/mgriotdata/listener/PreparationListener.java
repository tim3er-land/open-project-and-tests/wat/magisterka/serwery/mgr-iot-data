package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import pl.wat.mgr.witowski.mgriotdata.commons.dictionary.DictionaryCacheService;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentEntity;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentRuleEntity;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentUserEntity;
import pl.wat.mgr.witowski.mgriotdata.dto.dict.DictionaryItemDto;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.event.PreperationEvent;
import pl.wat.mgr.witowski.mgriotdata.listener.interfaces.BasicDataListener;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentRepository;
import pl.wat.mgr.witowski.mgriotdata.repository.agent.AgentUserRuleRepository;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Log4j2
@Service
public class PreparationListener extends BasicDataListener<PreperationEvent> {
    public final static String eventType = "PREPERATION";
    private final AgentRepository agentRepository;
    private final AgentUserRuleRepository agentUserRuleRepository;
    private final DictionaryCacheService dictionaryCacheService;

    @Autowired
    public PreparationListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher, AgentRepository agentRepository, AgentUserRuleRepository agentUserRuleRepository, DictionaryCacheService dictionaryCacheService) {
        super(objectMapper, applicationEventPublisher);
        this.agentRepository = agentRepository;
        this.agentUserRuleRepository = agentUserRuleRepository;
        this.dictionaryCacheService = dictionaryCacheService;
    }

    @Override
    @EventListener(condition = "#data.nextStep.equalsIgnoreCase('PREPERATION')")
    public void reveiceData(PreperationEvent data) {
        BasicEvent event = null;
        IotAgentEntity agent = agentRepository.findByIotAgtUidJoinUser(data.getAgentUid());
        if (agent == null || CollectionUtils.isEmpty(agent.getIotAgentUsersByIotAgtId())) {
            event = EventFactory.builder()
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.WARN_LEVEL)
                    .queueProcess(new LinkedList<>(Arrays.asList(EndLogListener.nextStep)))
                    .agentUid(data.getAgentUid())
                    .errors(null)
                    .inPayload(data.getInPayload())
                    .messageUid(data.getMessageUid())
                    .topic(data.getTopic())
                    .messageUid(data.getMessageUid())
                    .exceptionMessage(null)
                    .code("iot-agent-002-001")
                    .endLogPayload(data.getPayload())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build().createEvent();
            applicationEventPublisher.publishEvent(event);
            return;
        }
        Collection<IotAgentUserEntity> agentUserEntities = agent.getIotAgentUsersByIotAgtId();
        List<String> userUids = agentUserEntities.stream().map(iotAgentUserEntity -> iotAgentUserEntity.getIotUsrUid()).distinct().collect(Collectors.toList());
        List<IotAgentRuleEntity> ruleForUser = agentUserRuleRepository.getRuleForUser(userUids);
        if (CollectionUtils.isEmpty(ruleForUser)) {
            event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.INFO_LEVEL)
                    .queueProcess(new LinkedList<>(Arrays.asList(SaveRawDataListener.eventType, EndLogListener.nextStep)))
                    .saveData(extractPayload(data))
                    .agentUid(data.getAgentUid())
                    .topic(extractTopic(data.getPayload()))
                    .errors(null)
                    .messageUid(data.getMessageUid())
                    .inPayload(data.getInPayload())
                    .messageUid(data.getMessageUid()).exceptionMessage(null)
                    .code("iot-agent-000-000")
                    .endLogPayload(data.getPayload())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
        } else {
            List<DictionaryItemDto> rulesDict = dictionaryCacheService.getDictionaryItems("RULES");
            Map<String, DictionaryItemDto> ruleMap = rulesDict.stream().collect(Collectors.toMap(DictionaryItemDto::getItemCode, Function.identity()));
            List<IotAgentRuleEntity> filterRules = ruleForUser.stream().filter(iotAgentRuleEntity -> ruleMap.containsKey(iotAgentRuleEntity.getIotAgtRulCode())).collect(Collectors.toList());
            List<String> queueProcess = filterRules.stream()
                    .map(iotAgentRuleEntity -> iotAgentRuleEntity.getIotAgtRulCode())
                    .collect(Collectors.toList());
            List<String> processUids = filterRules.stream().map(IotAgentRuleEntity::getIotAgtRulUid).collect(Collectors.toList());
            queueProcess.add(StatusListener.eventType);
            queueProcess.add(EndLogListener.nextStep);
            event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(LogEnum.INFO_LEVEL)
                    .queueProcess(new LinkedList<>(queueProcess))
                    .saveData(extractPayload(data))
                    .agentUid(data.getAgentUid())
                    .topic(extractTopic(data.getPayload()))
                    .messageUid(data.getMessageUid())
                    .errors(null)
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code("iot-agent-000-000")
                    .endLogPayload(data.getPayload())
                    .processUids(new LinkedList<>(processUids))
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
        }
        applicationEventPublisher.publishEvent(event);
    }

    private String extractDataUid(String data) {
        try {
            HashMap hashMap = objectMapper.readValue(data, HashMap.class);
            if (hashMap.containsKey("messageUid")) {
                return objectMapper.writeValueAsString(hashMap.get("messageUid"));
            }
        } catch (Exception ex) {
            log.error("Error occured while parse data", ex);
        }
        return null;
    }

    private String extractTopic(String data) {
        try {
            HashMap hashMap = objectMapper.readValue(data, HashMap.class);
            if (hashMap.containsKey("topic")) {
                return (String) hashMap.get("topic");
            }
        } catch (Exception ex) {
            log.error("Error occured while parse data", ex);
        }
        return null;
    }

    private String extractPayload(PreperationEvent data) {
        try {
            HashMap hashMap = objectMapper.readValue(data.getPayload(), HashMap.class);
            if (hashMap.containsKey("payload")) {
                return objectMapper.writeValueAsString(hashMap.get("payload"));
            }
        } catch (Exception ex) {
            log.error("Error occured while parse data", ex);
        }
        return data.getPayload();
    }

}
