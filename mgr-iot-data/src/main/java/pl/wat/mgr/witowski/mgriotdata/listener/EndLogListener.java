package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.event.EndLogEvent;
import pl.wat.mgr.witowski.mgriotdata.listener.interfaces.BasicDataListener;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.LogEnum;
import pl.wat.mgr.witowski.mgriotdata.logs.utils.SubAgentLog;

import java.time.ZoneId;

@Log4j2
@Service
public class EndLogListener extends BasicDataListener<EndLogEvent> {
    public static final String nextStep = "ENDLOG";


    public EndLogListener(ApplicationEventPublisher applicationEventPublisher) {
        super(new ObjectMapper(), applicationEventPublisher);
    }

    @Override
    @EventListener(condition = "#endLogEvent.nextStep.equalsIgnoreCase('EndLog')")
    public void reveiceData(EndLogEvent endLogEvent) {
        endLogEvent.setEventList(addNewEventToList(nextStep, endLogEvent.getEventList()));
        switch (endLogEvent.getLogType()) {
            case ERROR_LEVEL:
                SubAgentLog.log(LogEnum.ERROR_LEVEL, nextStep, endLogEvent.getEndLogPayload()
                        , endLogEvent.getTopic(), endLogEvent.getAgentUid(), endLogEvent.getStart().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                        , endLogEvent.getCode(), endLogEvent.getEventList(), endLogEvent.getSendAgentUid(), endLogEvent.getDataType().getType(), endLogEvent.getExceptionMessage());
                break;
            case INFO_LEVEL:
                SubAgentLog.log(LogEnum.INFO_LEVEL, nextStep, endLogEvent.getEndLogPayload()
                        , endLogEvent.getTopic(), endLogEvent.getAgentUid(), endLogEvent.getStart().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                        , endLogEvent.getCode(), endLogEvent.getEventList(), endLogEvent.getSendAgentUid(), endLogEvent.getDataType().getType(), endLogEvent.getExceptionMessage());
                break;
            case WARN_LEVEL:
                SubAgentLog.log(LogEnum.WARN_LEVEL, nextStep, endLogEvent.getEndLogPayload()
                        , endLogEvent.getTopic(), endLogEvent.getAgentUid(), endLogEvent.getStart().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                        , endLogEvent.getCode(), endLogEvent.getEventList(), endLogEvent.getSendAgentUid(), endLogEvent.getDataType().getType(), endLogEvent.getExceptionMessage());
                break;
            default:
                SubAgentLog.log(LogEnum.DEBUG_LEVEL, nextStep, endLogEvent.getEndLogPayload()
                        , endLogEvent.getTopic(), endLogEvent.getAgentUid(), endLogEvent.getStart().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
                        , endLogEvent.getCode(), endLogEvent.getEventList(), endLogEvent.getSendAgentUid(), endLogEvent.getDataType().getType(), endLogEvent.getExceptionMessage());
                break;
        }
    }
}
