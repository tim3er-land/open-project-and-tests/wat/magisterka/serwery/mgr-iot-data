package pl.wat.mgr.witowski.mgriotdata.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.dao.AgentDataStatusDao;
import pl.wat.mgr.witowski.mgriotdata.event.BasicEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EndLogEvent;
import pl.wat.mgr.witowski.mgriotdata.event.EventFactory;
import pl.wat.mgr.witowski.mgriotdata.listener.interfaces.BasicDataListener;
import pl.wat.mgr.witowski.mgriotdata.repository.AgentDataStatusReposotory;

import java.util.UUID;

@Log4j2
@Service
public class StatusListener extends BasicDataListener<EndLogEvent> {
    public final static String eventType = "STATUS";
    private final AgentDataStatusReposotory agentDataStatusReposotory;

    @Autowired
    public StatusListener(ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher
            , AgentDataStatusReposotory agentDataStatusReposotory) {
        super(objectMapper, applicationEventPublisher);
        this.agentDataStatusReposotory = agentDataStatusReposotory;
    }

    @Override
    @EventListener(condition = "#data.nextStep.equalsIgnoreCase('STATUS')")
    public void reveiceData(EndLogEvent data) {
        BasicEvent event = null;
        try {
            AgentDataStatusDao.AgentDataStatusDaoBuilder builder = AgentDataStatusDao.builder();
            builder.messageStatusUid(UUID.randomUUID().toString())
                    .code(data.getCode())
                    .subCode(data.getSubCode())
                    .messageStatusType(data.getDataType().getType())
                    .agentUid(data.getAgentUid())
                    .errors(data.getErrors())
                    .messageUid(data.getMessageUid())
                    .dataInsert(data.getStart().toString());
            event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(data.getLogType())
                    .queueProcess(data.getQueueProcess())
                    .saveData(data.getEndLogPayload())
                    .endLogPayload(data.getEndLogPayload())
                    .agentUid(data.getAgentUid())
                    .topic(data.getTopic())
                    .sendAgentUid(data.getSendAgentUid())
                    .errors(null)
                    .messageUid(data.getMessageUid())
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code(data.getCode())
                    .saveData(data.getEndLogPayload())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
            agentDataStatusReposotory.save(builder.build());
        } catch (Exception ex) {
            log.error("Error occured save status for data: {} ", data.getMessageUid(), ex);
            event = EventFactory
                    .builder()
                    .sendAgentUid(data.getSendAgentUid())
                    .saveData(null)
                    .start(data.getStart())
                    .source(this)
                    .logType(data.getLogType())
                    .queueProcess(data.getQueueProcess())
                    .saveData(data.getEndLogPayload())
                    .endLogPayload(data.getEndLogPayload())
                    .agentUid(data.getAgentUid())
                    .topic(data.getTopic())
                    .sendAgentUid(data.getSendAgentUid())
                    .errors(null)
                    .messageUid(data.getMessageUid())
                    .inPayload(data.getInPayload())
                    .exceptionMessage(null)
                    .code("iot-agent-007-001")
                    .saveData(data.getEndLogPayload())
                    .eventList(addNewEventToList(eventType, data.getEventList()))
                    .dataType(data.getDataType())
                    .build()
                    .createEvent();
        }

        applicationEventPublisher.publishEvent(event);
    }
}
