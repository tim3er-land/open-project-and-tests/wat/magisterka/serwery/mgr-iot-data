package pl.wat.mgr.witowski.mgriotdata.logs.utils;

/**
 * @author: Piotr Witowski
 * @date: 04.02.2022
 * @project tim3erland-iot
 * Day of week: piątek
 */
public enum LogEnum {
    INFO_LEVEL,
    WARN_LEVEL,
    ERROR_LEVEL,
    DEBUG_LEVEL;
}
