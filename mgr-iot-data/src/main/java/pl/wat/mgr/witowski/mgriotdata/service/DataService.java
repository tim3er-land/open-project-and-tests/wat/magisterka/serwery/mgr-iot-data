package pl.wat.mgr.witowski.mgriotdata.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.mgriotdata.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.mgriotdata.dao.DataDao;
import pl.wat.mgr.witowski.mgriotdata.dao.RawDataDao;
import pl.wat.mgr.witowski.mgriotdata.dto.AgentDataDto;
import pl.wat.mgr.witowski.mgriotdata.repository.DataReposotory;
import pl.wat.mgr.witowski.mgriotdata.repository.RawDataReposotory;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class DataService {
    private final DataReposotory dataReposotory;
    private final RawDataReposotory rawDataReposotory;
    private final ObjectMapper objectMapper;

    @Autowired

    public DataService(DataReposotory dataReposotory, RawDataReposotory rawDataReposotory) {
        this.dataReposotory = dataReposotory;
        this.rawDataReposotory = rawDataReposotory;
        this.objectMapper = new ObjectMapper();
    }

    public List<AgentDataDto> getAgentData(String agentUid, Integer page, Integer limit) {
        try {
            List<DataDao> by = dataReposotory.getAgentData(agentUid, PageRequest.of(page, limit, Sort.Direction.DESC, "creationDate"));
            return by.parallelStream().map(agentDataDto -> {
                try {
                    return AgentDataDto.builder()
                            .data(objectMapper.readValue(agentDataDto.getPayload(), HashMap.class))
                            .messageUid(agentDataDto.getDataUid())
                            .messageDate(agentDataDto.getCreationDate().toString())
                            .build();
                } catch (Exception e) {
                    log.error("Error occured convert agent data", e);
                }
                return null;
            }).collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("error occured while getAgentData", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-data-001-001", "mgr-iot-data-001-001");
        }
    }

    public List<AgentDataDto> getAgentDataByTopic(String agentUid, String topicUid, Integer page, Integer limit) {
        try {
            List<DataDao> by = dataReposotory.getAgentDataByTopic(agentUid, topicUid, PageRequest.of(page, limit, Sort.Direction.DESC, "creationDate"));
            return by.parallelStream().map(agentDataDto -> AgentDataDto.builder()
                    .data(objectMapper.convertValue(agentDataDto.getPayload(), HashMap.class))
                    .messageUid(agentDataDto.getDataUid())
                    .messageDate(agentDataDto.getCreationDate().toString())
                    .build()).collect(Collectors.toList());
        } catch (Exception ex) {
            log.error("error occured while getAgentDataByTopic", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-data-002-001", "mgr-iot-data-002-001");
        }
    }

    public List<AgentDataDto> getAgentDataRaw(String agentUid, Integer page, Integer limit) {
        try {
            List<RawDataDao> by = rawDataReposotory.getAgentDataRaw(agentUid, PageRequest.of(page, limit, Sort.Direction.DESC, "creationDate"));
            return by.parallelStream().map(agentDataDto -> {
                try {
                    return AgentDataDto.builder()
                            .data(objectMapper.readValue(agentDataDto.getPayload(), HashMap.class))
                            .messageUid(agentDataDto.getDataUid())
                            .messageDate(agentDataDto.getCreationDate().toString())
                            .build();
                } catch (JsonProcessingException e) {
                    log.error("Error occured convert agent data", e);
                }
                return null;
            }).collect(Collectors.toList());

        } catch (Exception ex) {
            log.error("error occured while getAgentDataRaw", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-data-003-001", "mgr-iot-data-003-001");
        }
    }

    public List<AgentDataDto> getAgentDataByTopicRaw(String agentUid, String topicUid, Integer page, Integer limit) {
        try {
            List<RawDataDao> by = rawDataReposotory.getAgentDataByTopicRaw(agentUid, topicUid, PageRequest.of(page, limit, Sort.Direction.DESC, "creationDate"));
            return by.parallelStream().map(agentDataDto -> AgentDataDto.builder()
                    .data(objectMapper.convertValue(agentDataDto.getPayload(), HashMap.class))
                    .messageUid(agentDataDto.getDataUid())
                    .build()).collect(Collectors.toList());

        } catch (Exception ex) {
            log.error("error occured while getAgentDataByTopicRaw", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "mgr-iot-data-004-001", "mgr-iot-data-004-001");
        }
    }

}
