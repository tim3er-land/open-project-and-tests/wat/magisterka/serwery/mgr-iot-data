package pl.wat.mgr.witowski.mgriotdata.repository.agent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.wat.mgr.witowski.mgriotdata.dao.agent.IotAgentTopicEntity;

import java.util.List;

public interface AgentTopicRepository extends JpaRepository<IotAgentTopicEntity, Long> {

    @Query("select at from IotAgentTopicEntity at, IotAgentRuleEntity ar left join at.iotAgentByIotAgtId aa left join aa.iotAgentUsersByIotAgtId au " +
            "where at.iotAgtTopAct =1 and aa.iotAgtAct =1 and ar.iotUsrUid = au.iotUsrUid and ar.iotAgtRulUid = :ruleUid")
    List<IotAgentTopicEntity> getUserTopicByRule(@Param("ruleUid") String ruleUid);

    @Query("select at from IotAgentTopicEntity at left join at.iotAgentByIotAgtId aa left join aa.iotAgentUsersByIotAgtId au " +
            "where at.iotAgtTopAct = 1 and aa.iotAgtAct = 1 and au.iotUsrUid = :ruleUid")
    List<IotAgentTopicEntity> getUserTopicByUser(@Param("ruleUid") String ruleUid);

    IotAgentTopicEntity findByIotAgrTopUid(String topicUid);


}
