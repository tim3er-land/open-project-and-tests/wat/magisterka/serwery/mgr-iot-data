package pl.wat.mgr.witowski.mgriotdata.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MgrSecretDto {
    private String secretSign;
    private String secretDecrypt;

}
