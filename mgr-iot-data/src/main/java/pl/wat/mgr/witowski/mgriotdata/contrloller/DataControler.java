package pl.wat.mgr.witowski.mgriotdata.contrloller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import pl.wat.mgr.witowski.mgriotdata.dto.AgentDataDto;
import pl.wat.mgr.witowski.mgriotdata.service.DataService;

import java.util.List;

/**
 * @author: Piotr Witowski
 * @date: 25.12.2021
 * @project mgr-iot-data
 * Day of week: sobota
 */
@Slf4j
@CrossOrigin
@RestController
public class DataControler {

    private final DataService dataService;

    @Autowired
    public DataControler(DataService dataService) {
        this.dataService = dataService;
    }

    @GetMapping("/agent/data/{agentUid}")//001
    public ResponseEntity<List<AgentDataDto>> getAgentData(@PathVariable("agentUid") String agentUid,
                                                           @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                           @RequestParam(value = "limit", required = false, defaultValue = "50") Integer limit) {
        List<AgentDataDto> agentDataDtos = dataService.getAgentData(agentUid, page, limit);
        if (CollectionUtils.isEmpty(agentDataDtos)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(agentDataDtos);
    }

    @GetMapping("/agent/data/{agentUid}/topic/{topicUid}")//002
    public ResponseEntity<List<AgentDataDto>> getAgentDataByTopic(@PathVariable("agentUid") String agentUid, @PathVariable("topicUid") String topicUid,
                                                                  @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                                  @RequestParam(value = "limit", required = false, defaultValue = "50") Integer limit) {
        List<AgentDataDto> agentDataDtos = dataService.getAgentDataByTopic(agentUid, topicUid, page, limit);
        if (CollectionUtils.isEmpty(agentDataDtos)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(agentDataDtos);
    }

    @GetMapping("/agent/data/{agentUid}/raw")//003
    public ResponseEntity<List<AgentDataDto>> getAgentDataRaw(@PathVariable("agentUid") String agentUid,
                                                              @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                              @RequestParam(value = "limit", required = false, defaultValue = "50") Integer limit) {
        List<AgentDataDto> agentDataDtos = dataService.getAgentDataRaw(agentUid, page, limit);
        if (CollectionUtils.isEmpty(agentDataDtos)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(agentDataDtos);
    }

    @GetMapping("/agent/data/{agentUid}/topic/{topicUid}/raw")//004
    public ResponseEntity<List<AgentDataDto>> getAgentDataByTopicRaw(@PathVariable("agentUid") String agentUid, @PathVariable("topicUid") String topicUid,
                                                                     @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                                     @RequestParam(value = "limit", required = false, defaultValue = "50") Integer limit) {
        List<AgentDataDto> agentDataDtos = dataService.getAgentDataByTopicRaw(agentUid, topicUid, page, limit);
        if (CollectionUtils.isEmpty(agentDataDtos)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(agentDataDtos);
    }


}
