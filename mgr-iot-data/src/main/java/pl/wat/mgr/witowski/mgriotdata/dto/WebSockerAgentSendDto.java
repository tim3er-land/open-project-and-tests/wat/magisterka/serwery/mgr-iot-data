package pl.wat.mgr.witowski.mgriotdata.dto;

import lombok.Builder;

/**
 * @author: Piotr Witowski
 * @date: 24.12.2021
 * @project mgr-iot-hub
 * Day of week: piątek
 */
public class WebSockerAgentSendDto {
    private String agentName;
    private String messageContent;
    private String topic;
    private String messageUid;
    private String sendDate;


    @Builder
    public WebSockerAgentSendDto(String agentName, String messageContent, String topic, String messageUid, String sendDate) {
        this.agentName = agentName;
        this.messageContent = messageContent;
        this.topic = topic;
        this.messageUid = messageUid;
        this.sendDate = sendDate;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessageUid() {
        return messageUid;
    }

    public void setMessageUid(String messageUid) {
        this.messageUid = messageUid;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }
}
